#!/bin/bash
# configs
INSTALL_DIR_ROOT=/opt
INSTALL_DIR=$INSTALL_DIR_ROOT/pretty-hooks
SOURCE_DIR=./resources/pretty-hooks
LIBRARY_DIR=$SOURCE_DIR/library
INSTALL_LIBRARY_DIR=./resources/library
TRUE=1
FALSE=0
IS_FAIL=$FALSE
RESULT=$FALSE
DEBUG_MODE=$FALSE
#imports
source $LIBRARY_DIR/pretty-out.bash
source $LIBRARY_DIR/python-admin-modules.bash
source $LIBRARY_DIR/utils-program.bash
source $INSTALL_LIBRARY_DIR/tools-installer.bash
