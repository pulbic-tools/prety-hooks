#!/bin/bash
# configs
source configs.bash

# copy app at directory opt
check_command "cp -r resources/pretty-hooks $INSTALL_DIR_ROOT" "copy files successfully" "error in copy files" 
is_error_rollback
# add group
check_command_not_update_fail "groupadd pretty-hooks" "group pretty-hooks add successfully" "error at add group pretty-hooks"
# add users at group
check_command_not_update_fail "add_users_group pretty-hooks `users_system`" "add users at group successfully" "error at add users at group" 
# create link at directory in /usr/bin
check_command "ln -s $INSTALL_DIR/pretty-hooks.bash /usr/bin/pretty-hooks" "create command successfully" "error in create command" 
is_error_rollback
# set group
check_command_not_update_fail "chown :pretty-hooks -R $INSTALL_DIR" "set group" "error at set group"
# add permissions at directory
check_command_not_update_fail "chmod a+rx,g+w -R $INSTALL_DIR" "permissions changed successfully" "error changing permissions"
# set group in command
check_command_not_update_fail "chown :pretty-hooks -R /usr/bin/pretty-hooks" "set group in command" "error at set group in command"
# add permissions at command
check_command_not_update_fail "chmod g+wrx -R /usr/bin/pretty-hooks" "permissions changed successfully in command" "error changing permissions in command" 


