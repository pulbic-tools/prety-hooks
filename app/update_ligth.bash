#!/bin/bash
# configs
source configs.bash
# rm directory app
check_command_not_update_fail "rm -r $INSTALL_DIR/*" "remove old app successfully" "error remove old app" 
check_command_not_update_fail "rm -r $INSTALL_DIR/.version" "remove old version" "error remove old version" 
# copy app at directory opt
check_command_not_update_fail "cp -r resources/pretty-hooks/* $INSTALL_DIR" "copy files successfully" "error in copy files"
check_command_not_update_fail "cp -r resources/pretty-hooks/.version $INSTALL_DIR" "copy version files successfully" "error in copy version files"
# set group
check_command_not_update_fail "chown :pretty-hooks -R $INSTALL_DIR/*" "set group" "error at set group"
# add permissions at directory
check_command_not_update_fail "chmod a+rx,g+w -R $INSTALL_DIR/*" "permissions changed successfully" "error changing permissions"

