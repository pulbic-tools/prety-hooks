#!/bin/bash

#--------------------------------------------------------------
# prints colours
#--------------------------------------------------------------
RED=31
YELLOW=33
GREEN=32
BLUE=34

print_color(){
    echo -e "\e[$1m$2\e[0m"
}

in_red(){
    print_color $RED "$@"
}

in_green(){
    print_color $GREEN "$@"
}

in_yellow(){
    print_color $YELLOW "$@"
}

in_blue(){
    print_color $BLUE "$@"
}

print_line(){
    in_blue "_____________________________________________________________________________________________________"
}

print_double_line(){
    in_blue "====================================================================================================="

}

print_enter(){
    echo -e "\n\r"  
}


# print error message in colours
print_error(){
    echo " [$(in_red FAIL)] $@"
}

print_done(){
    echo " [$(in_green DONE)] $@"
}

print_info(){
    echo " [$(in_yellow INFO)] $@"
}

print_title(){
    echo -e "\n\r\e[1;34m $@\e[0m\n\r"
}

print_logo(){
    printf "%s\n" $' /\\_/\\\n( o.o )\n > ^ <\n--LEO--'
}

