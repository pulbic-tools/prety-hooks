#!/bin/bash

print_version(){
    if [[ $# -eq 1 ]]; then 
        REFF_DIR=$1
    else 
        local REFF_DIR=$INSTALL_DIR
    fi
    PROGRAM_VERSION=`cat $REFF_DIR/.version`
    echo -n "$PROGRAM_VERSION"
}

print_online_version(){
    cd /tmp
    rm -f /tmp/.version >/dev/null 2>&1
    wget https://gitlab.com/my-public-tools/pretty-hooks/-/raw/main/app/resources/pretty-hooks/.version >/dev/null 2>&1
    PROGRAM_ONLINE_VERSION=`cat /tmp/.version`
    echo -n "$PROGRAM_ONLINE_VERSION"
}

is_equal_online_version(){
    PROGRAM_VERSION=`print_version`
    PROGRAM_ONLINE_VERSION=`print_online_version`
    if [[ $PROGRAM_VERSION = $PROGRAM_ONLINE_VERSION ]]; then
       return $TRUE
    else
        return $FALSE
    fi
}


users_system(){
    cat /etc/passwd | grep /bin/bash | grep -o -e "^.*:x" | sed "s/:x//" | tr ':x\n' ' '
}

add_users_group(){
    GROUP=$1
    shift
    USERS=$@
    for USER in $USERS
    do
        gpasswd -a $USER $GROUP
    done
}

update_app(){
    cd /tmp
    rm -rf /tmp/pretty-hooks >/dev/null 2>&1
    git clone https://gitlab.com/my-public-tools/pretty-hooks.git >/dev/null 2>&1
    cd /tmp/pretty-hooks/app
    ./update_ligth.bash
}

print_program_version(){
    PROGRAM_NAME=`basename $0`
    PROGRAM_VERSION=`print_version $1`
    print_title "$PROGRAM_NAME $PROGRAM_VERSION"
}

print_update_version(){
    print_title update program from version `print_version $1` at version `print_version $2`
}

print_currently_updated(){
    print_title the application is currently updated
}

print_work_directory(){
    in_blue " work directory is $CODE_DIR"
}

verify_directory(){
     if [[ -d $1 ]]; then 
            print_done "$2"
        else 
            print_error "$3"
            IS_FAIL=$TRUE
        fi
}

check_command_not_update_fail(){
    [[ $DEBUG_MODE -eq $TRUE ]] && print_info EXECUTE COMMAND: $1
    $(eval "$1" >/dev/null 2>&1)
    if [[ $? -eq 0 ]]; then 
        print_done "$2"
    else 
        print_error "$3"
    fi
}

check_command(){
    [[ $DEBUG_MODE -eq $TRUE ]] && print_info EXECUTE COMMAND: $1
    $(eval "$1" >/dev/null 2>&1)
    if [[ $? -eq 0 ]]; then 
        print_done "$2"
    else 
        print_error "$3"
        IS_FAIL=$TRUE
    fi
}

check_command_show(){
    print_info EXECUTE COMMAND: $1
    check_command $@
}

set_work_directory(){
    while [ : ]; do
        case "$1" in
            -w | --work_directory)
                CODE_DIR=$3
                [[ $DEBUG_MODE -eq $TRUE ]] && print_info SET CODE_DIR: $CODE_DIR
                break
                ;;
            --) 
                break 
                ;;
            *) 
                shift
                ;;
        esac
    done
}

is_set_flag(){
    local FLAG=$1
    shift
    while [ : ]; do
        case "$1" in
            $FLAG)
                [[ $DEBUG_MODE -eq $TRUE ]] && print_info SET FLAG: $FLAG
                return $TRUE
                ;;
            --) 
                break 
                ;;
            *) 
                shift
                ;;
        esac
    done
    [[ $DEBUG_MODE -eq $TRUE ]] && print_info NOT SET FLAG: $FLAG
    return $FALSE
}


is_exist_directory_and_not_empty(){
    RESULT=$FALSE
    [[ -d $1 ]] && [[ ! -z `ls -A $1` ]] && RESULT=$TRUE
}

is_exist_file(){
    RESULT=$FALSE
    [[ -f $1 ]] && RESULT=$TRUE
}

is_exist_link(){
    RESULT=$FALSE
    [[ -L $1 ]] && RESULT=$TRUE
}

is_defined(){
    RESULT=$FALSE
    [[ -n $1 ]] && RESULT=$TRUE
}

charge_configuracion(){
    is_exist_file $1
    if [[ $RESULT -eq $FALSE ]]; then
        print_info "Unable to find file $1"
        return 1;
    else
        source $1
        if [[ $? -ne 0 ]]; then
            print_info "Unable to load file $1"
            return 1;
        else
             print_done "File $1 is loaded successfully"
             return 0
        fi
    fi
}


charge_configuracion_default(){
    RESULT=0
    charge_configuracion $2
    RESULT=$?
    if [[ $RESULT -ne 0 ]]; then
        charge_configuracion $1
        RESULT=$?
    return $RESULT
    fi
}

question_yes_no(){
    echo -e  "\n"
    echo "$1"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) RESULT=$TRUE; break;;
            No ) RESULT=$FALSE; break;;
        esac
    done
    echo -e  "\n"
    return $RESULT
}
