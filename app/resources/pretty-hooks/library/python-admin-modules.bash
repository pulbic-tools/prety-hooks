#!/bin/bash

is_already_installed(){
    IS_EXIST=$FALSE
    pip freeze | grep $1== >/dev/null 2>&1
    [[ $? -eq 0 ]] && IS_EXIST=$TRUE
    return $IS_EXIST
}

# install python librarie
install_dependence(){
    is_already_installed $1
    IS_EXIST=$?
    if [[ $IS_EXIST -ne $TRUE ]]; then
        print_info "$1 are not installed, proceed to install it"
        yes | pip install $1 >/dev/null 2>&1
        if [[ $? -eq 0 ]]; then 
            print_done "$1 are installed"
        else 
            print_error "failed to install $1"
            IS_FAIL=$TRUE
        fi
    fi
}

# uninstall python librarie
uninstall_dependence(){
    is_already_installed $1
    IS_EXIST=$?
    if [[ $IS_EXIST -eq $TRUE ]]; then
        print_info "$1 are installed, proceed to uninstall it"
        yes | pip uninstall $1 >/dev/null 2>&1
        if [[ $? -eq 0 ]]; then 
            print_done "$1 are uninstalled"
        else 
            print_error "failed to uninstall $1"
            IS_FAIL=$TRUE
        fi

    fi
}

# install a list of libraries
install_dependencies(){
    for dependence in $@; do
        install_dependence $dependence
    done
}

# uninstall a list of libraries
uninstall_dependencies(){
    for dependence in $@; do
        uninstall_dependence $dependence
    done
}


# execute all comprobations
execute_comprobations_personalize(){
ORIGEN=$1
shift
for dependence in $@; do
    [[ "$IS_FAIL" -eq $TRUE ]] && break
    VARIABLE_SEARCHING=$dependence'_'$ORIGEN
    # change at uppercase
    VARIABLE_SEARCHING=`echo ${VARIABLE_SEARCHING^^}`
    if [[ -n ${!VARIABLE_SEARCHING} ]]; then
        COMMAND=`echo ${!VARIABLE_SEARCHING} $CODE_DIR`
    else
        COMMAND="$dependence $CODE_DIR"
    fi
    $(eval "$COMMAND" >/dev/null 2>&1)
    if [[ $? -eq 0 ]]; then 
        print_done "$dependence are pass correctly"
    else 
        print_error "$dependence are failed, please check manually"
        IS_FAIL=$TRUE
    fi
done
}


# execute all comprobations
execute_comprobations(){
for dependence in $@; do
    [[ "$IS_FAIL" -eq $TRUE ]] && break
    $dependence $CODE_DIR >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then 
        print_done "$dependence are pass correctly"
    else 
        print_error "$dependence are failed, please check manually"
        IS_FAIL=$TRUE
    fi
done
}

# execute all comprobations
execute_comprobations_not_break(){
for dependence in $@; do
    $dependence $CODE_DIR >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then 
        print_done "$dependence are pass correctly"
    else 
        print_error "$dependence are failed, please check manually"
        IS_FAIL=$TRUE
    fi
done
}

# execute all comprobations
execute_comprobations_vervose(){
for dependence in $@; do
    print_enter
	print_double_line
    in_blue "$dependence"
    print_double_line
    [[ $DEBUG_MODE -eq $TRUE ]] && print_info EXECUTE COMMAND: "$dependence -v $CODE_DIR"
    eval "$dependence -v $CODE_DIR"
    if [[ $? -eq 0 ]]; then
        print_double_line
        print_done "$dependence are pass correctly"
    else
        print_double_line
        print_error "$dependence are failed, please check manually"
        IS_FAIL=$TRUE
    fi
    print_double_line
    print_enter
done
}

# show error in the environment
error_environment(){
    print_error "Do not can find the enviroment, please check $VENV_DIR_DEFAULT or set \$VENV_DIR"
    exit 1
}

init_environment(){
    SILENT=$FALSE
    [[ ! -z "$1" ]] && SILENT=$1
    [[ "$SILENT" -eq $FALSE ]] && print_title "INIT CHECK OF CODE, THIS MAY TAKE SOME MINUTES PLEASE WAIT"
    [[ -z "$VENV_DIR" ]] && VENV_DIR=$VENV_DIR_DEFAULT
    source $VENV_DIR/bin/activate
    IS_ACTIVATE=$?
    [[ $IS_ACTIVATE -ne 0 ]] && error_environment
}

finalize_environment(){
    SILENT=$FALSE
    [[ ! -z "$1" ]] && SILENT=$1
    deactivate
    [[ "$SILENT" -eq $FALSE ]] && print_title "RESULT OF PROCESS:"
    if [[ $IS_FAIL -eq $TRUE ]]; then
        print_error "some check failed, please chech manually and try again"
        [[ "$SILENT" -eq $FALSE ]] && print_title "ABORT TASK:"
        exit 1
    else
        print_done "all check done, proceed task"
        [[ "$SILENT" -eq $FALSE ]] && print_title "INIT TASK:"
        exit 0
    fi
}