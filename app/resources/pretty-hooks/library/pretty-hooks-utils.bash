#!/bin/bash


pretty-hooks-initial-config(){
    charge_configuracion_default $DEFAULT_CONFIG_FILE $CODE_DIR/$CONFIG_FILE
    set_work_directory "$@"
    print_work_directory
    pretty-hooks-automatic-update
    if [[ $RESULT -eq $TRUE ]]; then
        print_info "This application has been successfully updated, need to restart"
        IS_FAIL=$TRUE
        exit 1
    fi
}

pretty-hooks-automatic-update(){
    # verify automatic upgrade
    [[ $AUTOMATIC_UPDATE -eq $TRUE ]] && pretty-hooks-update
}

pretty-hooks-create-task-file(){
    echo "#!/bin/bash" > .git/hooks/$HOOKS_TYPE
    [[ $? -ne 0 ]] && IS_FAIL=$TRUE
    echo "# hook created for $PROGRAM_NAME $PROGRAM_VERSION" >> .git/hooks/$HOOKS_TYPE
    [[ $? -ne 0 ]] && IS_FAIL=$TRUE
    echo "exec < /dev/tty" >> .git/hooks/$HOOKS_TYPE
    [[ $? -ne 0 ]] && IS_FAIL=$TRUE
    echo "$PROGRAM_NAME --validate --work_directory $CODE_DIR" >> .git/hooks/$HOOKS_TYPE
    [[ $? -ne 0 ]] && IS_FAIL=$TRUE && exit 1
    echo "exec <&-" >> .git/hooks/$HOOKS_TYPE
    [[ $? -ne 0 ]] && IS_FAIL=$TRUE
    exit 0
}

pretty-hooks-install(){
    init_environment $TRUE
    verify_directory ".git" "git is instaled in the selected directory" "not intalle git in the selected directory"
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment $TRUE
    install_dependencies ${DEPENDENCIES_LINT[@]}
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment $TRUE
    install_dependencies ${DEPENDENCIES_CHECK[@]}
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment $TRUE
    check_command "pretty-hooks-create-task-file" "task file created successfully" "error in crete task file" 
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment $TRUE
    check_command "chmod +x .git/hooks/$HOOKS_TYPE" "permissions change" "error in permissions change"
    local COMMAND="cp"
    [[ $LINKED -eq $TRUE ]] && COMMAND="ln -s"
    check_command "$COMMAND $INSTALL_DIR/config/pyproject.toml ./pyproject.toml" "configure ./pyproject.toml" "error in configure ./pyproject.toml"
    check_command "$COMMAND $INSTALL_DIR/config/setup.cfg ./setup.cfg" "configure ./setup.cfg" "error in configure ./setup.cfg"
    if [[ ! $CODE_DIR = "." ]]; then
        check_command "$COMMAND $INSTALL_DIR/config/pyproject.toml $CODE_DIR/pyproject.toml" "configure $CODE_DIR/pyproject.toml" "error in configure $CODE_DIR/pyproject.toml"
        check_command "$COMMAND $INSTALL_DIR/config/setup.cfg $CODE_DIR/setup.cfg" "configure $CODE_DIR/setup.cfg" "error in configure $CODE_DIR/setup.cfg" 
    fi
    finalize_environment $TRUE
}

pretty-hooks-uninstall(){
    init_environment $TRUE
    verify_directory ".git" "git is instaled in the selected directory" "not intalle git in the selected directory"
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment $TRUE
    uninstall_dependencies ${DEPENDENCIES_LINT[@]}
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment $TRUE
    uninstall_dependencies ${DEPENDENCIES_CHECK[@]}
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment $TRUE
    check_command "rm .git/hooks/$HOOKS_TYPE" "task file deleted successfully" "error in delete task file"
    check_command "unlink ./pyproject.toml" "desconfigure ./pyproject.toml" "error in desconfigure ./pyproject.toml"
    check_command "unlink ./setup.cfg" "desconfigure ./setup.cfg" "error in desconfigure ./setup.cfg"
    if [[ ! $CODE_DIR = "." ]]; then
    	check_command "unlink $CODE_DIR/pyproject.toml" "desconfigure $CODE_DIR/pyproject.toml" "error in desconfigure $CODE_DIR/pyproject.toml"
        check_command "unlink $CODE_DIR/setup.cfg" "desconfigure $CODE_DIR/setup.cfg" "error in desconfigure $CODE_DIR/setup.cfg" 
    fi
    finalize_environment $TRUE
}

pretty-hooks-lint(){
    init_environment $TRUE
    install_dependencies ${DEPENDENCIES_LINT[@]}
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment
    execute_comprobations_vervose ${DEPENDENCIES_LINT[@]}
    finalize_environment $TRUE
}

pretty-hooks-lint-question(){
    RESULT=$FALSE
    question_yes_no "You want to solve this problem by performing lint?"
    if [[ $RESULT -eq $TRUE ]]; then
        IS_FAIL=$FALSE  
        #execute_comprobations ${DEPENDENCIES_LINT[@]}
        execute_comprobations_personalize lint ${DEPENDENCIES_LINT[@]}
        # execute_comprobations_vervose ${DEPENDENCIES_LINT[@]}
        LINTING=$TRUE
        git status
    else
        IS_FAIL=$TRUE
    fi
}

pretty-hooks-gitadd-question(){
    question_yes_no "You want to perform git add?"
    if [[ $RESULT -eq $TRUE ]]; then
        git add .
        git status
        IS_FAIL=$FALSE
    else
        IS_FAIL=$TRUE
    fi
}

pretty-hooks-check(){
    init_environment $TRUE
    install_dependencies ${DEPENDENCIES_CHECK[@]}
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment
    execute_comprobations_vervose ${DEPENDENCIES_CHECK[@]}
    finalize_environment $TRUE

}

pretty-hooks-validate(){
    LINTING=$FALSE
    init_environment
    install_dependencies ${DEPENDENCIES_LINT[@]}
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment
    execute_comprobations_personalize validate ${DEPENDENCIES_LINT[@]}
    [[ $IS_FAIL -eq $TRUE ]] && [[ $AUTOMATIC_LINT -eq $TRUE ]] && pretty-hooks-lint-question && 
    [[ $LINTING -eq $TRUE ]] && [[ $AUTOMATIC_ADD -eq $TRUE ]] && pretty-hooks-gitadd-question
    install_dependencies ${DEPENDENCIES_CHECK[@]}
    [[ $IS_FAIL -eq $TRUE ]] && finalize_environment
    execute_comprobations_personalize validate ${DEPENDENCIES_CHECK[@]}
    finalize_environment
}


pretty-hooks-update(){
    RESULT=$FALSE
    is_equal_online_version
    if [[ $? -eq $FALSE ]]; then
        update_app
        RESULT=$TRUE
        return $RESULT
    else
        print_currently_updated
    fi
}

pretty-hooks-help(){
    print_program_version
    print_line         
    echo -e '\n'\
        'Test python apliations for erros lint, is posible install into git hooks\n'\
        'for automatize this process\n'\
        '\n'\
        'Usage: pretty-hooks [OPTION]\n'\
        '       pretty-hooks -i [--linked]\n'\
        '       pretty-hooks -(i|d|l|c|a) [--work_directory DIRECTORY]\n'\
        '\n'\
        'Options:\n'\
        '-h, --help               Show this dialog\n'\
        '-v, --version            Show version of the program\n'\
        '-u, --update             Update applicaion from the reposiory\n'\
        '-i, --install            Install pretty-hooks into git hooks\n'\
        '-d, --uninstall          Unistall pretty-hooks into git hooks\n'\
        '-l, --lint               Correct problems lint into program\n'\
        '-c, --check              Check consistence into program\n'\
        '-a, --validate           Check consistence and Correct problems into program\n'\
        '-w, --work_directory     Set work directory of program from the repository directory\n'\
        '--linked                 When installing in the repository, instead of copying the configuration\n'\
        '                         files, it creates symbolic links to them.\n'\
        '\n'\
        'Examples:\n'\
        'pretty-hooks --check --work_directory expandweb3be/expandweb3be\n'\
        'pretty-hooks --lint --work_directory expandweb3be/expandweb3be\n'\
        'pretty-hooks --install --work_directory expandweb3be/expandweb3be\n'\
        '\n'\
        'Thank you very much for using this application\n'\
        'For more information consult our repository: <https://gitlab.com/my-public-tools/pretty-hooks>\n'

    print_logo
}
