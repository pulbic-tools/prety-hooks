#!/bin/bash

# CONFIGS

INSTALL_DIR=/opt/pretty-hooks
TRUE=1
FALSE=0
IS_FAIL=$FALSE
CODE_DIR=.
LINKED=$FALSE
DEBUG_MODE=$FALSE
CONFIG_FILE=pretty-hooks.config
DEFAULT_CONFIG_FILE=$INSTALL_DIR/$CONFIG_FILE
PROGRAM_NAME=`basename $0`

# IMPORTS

source $INSTALL_DIR/library/pretty-out.bash
source $INSTALL_DIR/library/python-admin-modules.bash
source $INSTALL_DIR/library/utils-program.bash
source $INSTALL_DIR/library/pretty-hooks-utils.bash

# EXECUTE PROGRAM 

VALID_ARGS=$(getopt -o idlcauvhw: --long install,uninstall,lint,check,validate,update,version,help,work_directory,linked -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
  case "$1" in
    -i | --install)
        pretty-hooks-initial-config "$@"
        is_set_flag "--linked" "$@"
        [[ $? -eq $TRUE ]] && LINKED=$TRUE
        pretty-hooks-install
        break
        ;;
    -d | --uninstall)
        pretty-hooks-initial-config "$@"
        pretty-hooks-uninstall
        break
        ;;
    -l | --lint)
        pretty-hooks-initial-config "$@"
        pretty-hooks-lint
        break
        ;;
    -c | --check)
        pretty-hooks-initial-config "$@"
        pretty-hooks-check
        break
        ;;
    -a | --validate)
        pretty-hooks-initial-config "$@"
        pretty-hooks-validate
        break
        ;;
    -u | --update)
        pretty-hooks-update
        break
        ;;
    -v | --version)
        print_program_version
        break
        ;;
    -h | --help)
        pretty-hooks-help
        break
        ;;
    --) shift; 
        break 
        ;;
  esac
done

