#!/bin/bash
# configs
source configs.bash

print_title "UPDATE PRETTY-HOOKS `print_version $SOURCE_DIR`"

# version intalled update at actual version
OLD_VERSION=`print_version $INSTALL_DIR`
NEW_VERSION=`print_version $SOURCE_DIR`
if [[ $OLD_VERSION = $NEW_VERSION ]]; then
    print_done "SISTEM ALREADY INSTALL IN VERSION $NEW_VERSION"
else
    print_update_version $INSTALL_DIR $SOURCE_DIR
    # check that permissions are root
    ./uninstall.bash
    ./install.bash
    print_title FINISHED UPDATE
fi

exit 0