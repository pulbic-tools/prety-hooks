#Changelog

Control of changes made in the different versions
Change description format based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
Version numbering based on [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

The versions will be listed from the most current to the oldest, at the end of the document there will be a list with references to all versions

* Version numbering **A.B.C**:
+ **A** will indicate if there are new categories
+ **B** the incorporation of new courses
+ **C** minor changes, name or order, note taking within the course

## In the next version

### Added
### Changed
### Removed
## [UNRELEASED]

### Added

### Changed
### Removed

## [v1.3.1] - 2023-01-31

### Added

- Add help of pretty-hooks command
- Add a system usage section in the readme
- Add instructions of installing
- Add usage information
- Comprehensive translation of the project into English
- Add badge and improve the aesthetics of the main page
- Add config files
- Add automatic updates

## [v1.0.1] - 2022-12-01

### Added

- Improve the information provided by the installer
- Fixed --lint and --check options
- The execution of the checks ends at the first error

## [v1.0.0] - 2022-11-29

### Added

- Creation of installation script
- Creation of pretty-hooks script to manage project hooks

## [v0.0.1] - 2022-11-29

### Added

- Project creation
- Selection of version control and versioning methodology
- Selection of the collaboration methodology
- Selection of the type of license


[Unreleased]: https://gitlab.com/pulbic-tools/compare/v0.0.1...master
[v1.0.0]: https://gitlab.com/pulbic-tools/pretty-hooks/-/tags/v1.0.0
[v0.0.1]: https://gitlab.com/pulbic-tools/pretty-hooks/-/tags/v0.0.1